Here is how you can help!

 * Anyone can:
   * Provide links to tests that people have posted elsewhere.
   * Suggest improvements to this documentation, whether in content or layout, 
     by posting an [issue][issues], or...
   * [Create a branch][branch], and make a [merge request][merge] with your
     changes made.
   * Ask VR developers whether they plan Linux support, if it's not listed here.
 * If you have access to a VR headset, you can:
   * Test software or hardware for Linux VR compatibility.
     * Whether it works fine, works but has issues, or doesn't work at all, you
       can post an [issue][issues] for it. After that, we can also add you as
       a maintainer, if you wished to edit the README directly.
 * If you don't have access to a VR headset, you can:
   * Gift a VR application to someone who does, and is willing to test in Linux.
     * You should both be aware of the [Steam refund policy][policy].

[issues]: https://gitlab.com/yaomtc/VR-on-Linux/issues
[branch]: https://docs.gitlab.com/ee/gitlab-basics/create-branch.html
[merge]: https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html
[policy]: https://store.steampowered.com/steam_refunds